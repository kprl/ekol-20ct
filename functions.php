<?php

date_default_timezone_set('Europe/Stockholm');
setlocale(LC_TIME,'sv_SE');

// Enqueue parent theme styles and child theme stylesheet
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

if ( ! function_exists( 'pre_r' ) ) {
  function pre_r( $val, $die = 0 ) {
    echo "<pre>";
    print_r( $val );
    echo "</pre>";
    if ( $die == 1 ) { die(); }
  }
}

/**
 *
 * Enqueue stylesheet to Block Editor as well
 * (good for child themes where you're just adding the palette CSS)
 * Otherwise enqueue a different sheet for the Block Editor
 *
 */
function aa_add_block_editor_stylesheet() {
	// Add support for editor styles.
	add_theme_support( 'editor-styles' );

	// Enqueue editor styles.
	add_editor_style( 'editor.css' );
}
add_action( 'after_setup_theme', 'aa_add_block_editor_stylesheet' );

// För en gemensam menykategori i adminlägets vänsterspalt.
function ekol_add_admin_menu() {

  add_menu_page(
    __('Ek Online', 'twentytwenty'),
    __('Ek Online', 'twentytwenty'),
    'manage_options',
    'ekol-options',
    '',
    get_stylesheet_directory_uri() . '/logo_low_16x16.png',
    3
    );

	//Inställningar och information
  add_submenu_page(
    'ekol-options',
    __('Inställningar', 'twentytwenty'),
    __('Inställningar', 'twentytwenty'),
    'manage_options',
    'ekol-options',
    'ekol_options_page'
    );

}
add_action( 'admin_menu', 'ekol_add_admin_menu' );

// Including CPT:s
require_once('cpts/ekol_educations.php');
require_once('cpts/ekol_external_systems.php');

// Anpassad färgpalett!
function ekol_block_editor_palette() {
	add_theme_support(
		'editor-color-palette',
		[
			[
				'name'  => esc_html__( 'Primär', 'ekol_20ct' ),
				'slug'  => 'color-ekol-main',
				'color' => '#e76824',
			],
			[
				'name'  => esc_html__( 'Sekundär', 'ekol_20ct' ),
				'slug'  => 'color-ekol-second',
				'color' => '#1f2c2f',
			],
			[
				'name'  => esc_html__( 'Tre', 'ekol_20ct' ),
				'slug'  => 'color-ekol-third',
				'color' => '#293232',
			],
			[
				'name'  => esc_html__( 'Fyra', 'ekol_20ct' ),
				'slug'  => 'color-ekol-forth',
				'color' => '#1c1231',
			],
		]
	);
}
add_action( 'after_setup_theme', 'ekol_block_editor_palette' );

function ekol_acf_init() {

  /** BLOCKS **/
  // check function exists
  if ( function_exists('acf_register_block') ) {

    acf_register_block(array(
      'name'            => 'ekol_external_systems',
      'title'           => __('EK Online - Externa system'),
      'description'		  => __('Block för att visa externa system.'),
      'render_template' => get_stylesheet_directory() . '/blocks/block-ekol_external_systems.php',
      'mode'            => 'preview',
      'align'           => 'wide',
      'category'        => 'layout',
      'icon'            => 'vault',
      'keywords'        => array( 'ek', 'externa system', 'external systems', 'kprl' ),
    ));

		acf_register_block(array(
      'name'            => 'ekol_education',
      'title'           => __('EK Online - Undervisning'),
      'description'		  => __('Block för att visa undervisningar.'),
      'render_template' => get_stylesheet_directory() . '/blocks/block-ekol_education.php',
      'mode'            => 'preview',
      'align'           => 'wide',
      'category'        => 'layout',
      'icon'            => 'vault',
      'keywords'        => array( 'ek', 'undervisning', 'education', 'kprl' ),
    ));

  }

}
add_action('acf/init', 'ekol_acf_init');

// Build a custon meny options for external logins
add_filter( 'wp_nav_menu_items', 'your_custom_menu_item', 10, 2 );
function your_custom_menu_item ( $items, $args ) {
	if ($args->theme_location == 'primary') {
		$items .= '<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children"><a href="#">Externa inloggningar</a><span class="icon"></span>';
		$items .= '<ul id="submenu_externa_inloggningar" class="sub-menu">';

		$exsArr = return_ekol_external_systems();
		foreach ($exsArr as $key => $exs) {
			if ( $exs['login'] ) {
				$items .= '<li class="menu-item menu-item-type-post_type menu-item-object-page"><a target="_blank" href="' . $exs['login'] . '">Loggin ' . $exs['title'] . '</a></li>';
			}
		}

		$items .= '</ul>';
		$items .= '</li>';
	}
	return $items;
}

function return_ekol_education( $antal = null, $orderby = "id", $order = "ASC" ) {

  $arr = array();

  if ( !$antal ) {
    $antal = -1;
  }

  $eduArr = get_posts(array(
    'post_type' 		  => 'ekol_education',
    'post__in'        => $arr,
    'posts_per_page'  => $antal,
    'order'           => $order,
    'status'          => 'published',
  ));

	$educationArr = array();

  foreach ($eduArr as $key => $edu) {
    $educationArr[$key]['ID']            = $edu->ID;
    $educationArr[$key]['title']         = $edu->post_title;
    $educationArr[$key]['content']       = wpautop( $edu->post_content );
    $educationArr[$key]['excerpt']       = false;
    $educationArr[$key]['thumbnail_id']  = get_post_thumbnail_id( $edu->ID );
    $educationArr[$key]['link']          = get_post_permalink( $edu->ID );

    $fields = get_fields($edu->ID);

    if ( $fields['utdrag'] ) {
      $educationArr[$key]['excerpt']     = $fields['utdrag'];
    }

    $fields = array();
  }

  return $educationArr;

}

function return_ekol_external_systems( $antal = null, $orderby = "id", $order = "ASC" ) {

  $arr = array();

  if ( !$antal ) {
    $antal = -1;
  }

  $exsArr = get_posts(array(
    'post_type' 		  => 'ekol_external_system',
    'post__in'        => $arr,
    'posts_per_page'  => $antal,
    'orderby'         => $orderby,
    'order'           => $order,
    'status'          => 'published',
  ));

	$extersysArr = array();

  foreach ($exsArr as $key => $exs) {
    $extersysArr[$key]['ID']            = $exs->ID;
    $extersysArr[$key]['title']         = $exs->post_title;
    $extersysArr[$key]['content']       = wpautop( $exs->post_content );
    $extersysArr[$key]['login']       	= false;
    $extersysArr[$key]['thumbnail_id']  = get_post_thumbnail_id( $exs->ID );
    $extersysArr[$key]['link']          = get_post_permalink( $exs->ID );

		$fields = get_fields($exs->ID);
		if ( is_array($fields['extern_inloggning']) ) {
      $extersysArr[$key]['login']     	= $fields['extern_inloggning']['url'];
    }

    $fields = array();
  }

  return $extersysArr;

}
