<?php

  if ( ! function_exists('cpt_ekol_education') ) {

    // Register Custom Post Type
    function cpt_ekol_education() {

    	$labels = array(
    		'name'                  => _x( 'Utbildningar', 'Post Type General Name', 'ekol_20ct' ),
    		'singular_name'         => _x( 'Utbildning', 'Post Type Singular Name', 'ekol_20ct' ),
    		'menu_name'             => __( 'Utbildningar', 'ekol_20ct' ),
    		'name_admin_bar'        => __( 'Utbildning', 'ekol_20ct' ),
    	);

      $rewrite = array(
    		'slug'                  => 'utbildning',
    		'with_front'            => true,
    		'pages'                 => false,
    		'feeds'                 => true,
    	);

    	$args = array(
    		'label'                 => __( 'Utbildning', 'ekol_20ct' ),
    		'description'           => __( 'Ekonomikontoret Online - Utbildning', 'ekol_20ct' ),
    		'labels'                => $labels,
    		'supports'              => array( 'title', 'editor', 'thumbnail' ),
    		'taxonomies'            => array( 'category', 'post_tag' ),
    		'hierarchical'          => false,
    		'public'                => true,
    		'show_ui'               => true,
    		'show_in_menu'          => 'ekol-options',
    		'show_in_admin_bar'     => true,
    		'show_in_nav_menus'     => true,
    		'can_export'            => true,
    		'has_archive'           => true,
    		'exclude_from_search'   => false,
        'publicly_queryable'    => true,
    		'rewrite'               => $rewrite,
    		'capability_type'       => 'page',
    		'show_in_rest'          => true,
    	);
    	register_post_type( 'ekol_education', $args );

    }
    add_action( 'init', 'cpt_ekol_education', 0 );

  }
