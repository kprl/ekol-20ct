<?php

  if ( ! function_exists('cpt_ekol_external_system') ) {

  // Register Custom Post Type
  function cpt_ekol_external_system() {

  	$labels = array(
  		'name'                  => _x( 'Externa system', 'Post Type General Name', 'ekol_20ct' ),
  		'singular_name'         => _x( 'Externt system', 'Post Type Singular Name', 'ekol_20ct' ),
  		'menu_name'             => __( 'Externa system', 'ekol_20ct' ),
  		'name_admin_bar'        => __( 'Externa system', 'ekol_20ct' ),
  	);
  	$args = array(
  		'label'                 => __( 'Externt system', 'ekol_20ct' ),
  		'description'           => __( 'Ekonomikontoret Online - Externa system', 'ekol_20ct' ),
  		'labels'                => $labels,
  		'supports'              => array( 'title', 'editor', 'thumbnail' ),
  		'taxonomies'            => array( 'category', 'post_tag' ),
  		'hierarchical'          => false,
  		'public'                => true,
  		'show_ui'               => true,
  		'show_in_menu'          => 'ekol-options',
  		'show_in_admin_bar'     => true,
  		'show_in_nav_menus'     => true,
  		'can_export'            => true,
  		'has_archive'           => true,
  		'exclude_from_search'   => false,
  		'publicly_queryable'    => true,
  		'capability_type'       => 'page',
  		'show_in_rest'          => true,
  	);
  	register_post_type( 'ekol_external_system', $args );

  }
  add_action( 'init', 'cpt_ekol_external_system', 0 );

  }
