<div class="row">

<?php
foreach ($educationArr as $key => $education):
  $ubidkey = $block['id'] . "-" . $key;
?>

  <div class="col-12 col-md-6">

    <div class="card border-0">
      <!-- <img src="<?php echo get_the_post_thumbnail_url($education['ID'], 'medium'); ?>" class="card-img-top" alt="<?php echo $education['title']; ?>"> -->
      <div class="card-body">
        <h3 class="card-title"><?php echo $education['title']; ?></h3>
        <div class="card-text">
          <?php echo $education['excerpt']; ?>
          <div class="wp-block-button">
            <a class="wp-block-button__link" href="<?php echo $education['link']; ?>" style="border-radius:25px" target="_top" rel="noreferrer noopener">Läs mer</a>
          </div>
        </div>
      </div>
    </div>

  </div>

<?php endforeach; ?>

</div>
