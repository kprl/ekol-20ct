<?php
/**
 * Block Name: Ek Online - Utbildningar
 */

// get image field (array)
$fields = get_fields();
$fields['ekol_education-layout'] = 'plain';

if( $fields ):

  $id = 'ekol_education-' . $block['id'];

  $align_class  = $block['align'] ? 'align' . $block['align'] : '';
  if ( array_key_exists('className', $block) ) {
    $css_class  = $block['className'];
  } else {
    $css_class  = '';
  }


  $educationArr = return_ekol_education();

  $layout_folder = get_stylesheet_directory() . '/blocks/ekol_education-layouts/';

?>

  <div id="<?php echo $id; ?>" class="ekol_education <?php echo $align_class; ?> <?php echo $css_class; ?>">

    <?php
      if ( is_admin() ):
        include $layout_folder . 'admin.php';
      else:

        // plain
        // card-horizonta
        // card-group
        // card-deck
        // card-columns
        // contact-expand

        include $layout_folder . $fields['ekol_education-layout'] . '.php';

      endif;
    ?>

  </div>

  <?php

endif;
