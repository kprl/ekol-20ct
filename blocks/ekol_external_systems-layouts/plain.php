<div class="educations">

  <?php
  $i = 1;
  foreach ($educationArr as $key => $education):
    $ubidkey = $block['id'] . "-" . $key;
  ?>

  <div class="education_<?php echo $i; ?>">
    <?php echo get_the_post_thumbnail_url($education['ID'], 'medium'); ?>
    <h3 class="education-title"><?php echo $education['title']; ?></h3>
    <?php echo $education['content']; ?>
  </div>

  <?php
    $i++;
  endforeach; 
  ?>

</div>
